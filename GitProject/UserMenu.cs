﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitProject
{
    public partial class UserMenu : Form
    {
        private string _WelcomeName;
        public UserMenu(string WelcomeName)
        {
            _WelcomeName = WelcomeName;
            InitializeComponent();
            if (_WelcomeName == "Admin")
            {
                Console.WriteLine("Admin Menu Was Loaded!");
            }
            else
            {
                Console.WriteLine("User Menu Was Loaded!");
            }
            WelcomeLbl.Text += _WelcomeName;
        }
    }
}
