﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitProject
{
    
    class LoginFormDinamic
    {
        private bool ExsitName;

        public bool Logon = false;
        public bool RegisterTime = false;
        private Accounts accounts = new Accounts();
        public void New(TextBox userTxt, TextBox passTxt) // Clear Filds
        {
            passTxt.Text = "";
            userTxt.Text = "";
        }
        public void Info()
        {
            string InfoText = "This is the login form, if you do not have an account please register";
            MessageBox.Show(InfoText,"Information");
        } //Get Info
        public void RegisterBtn(TextBox usertxt, TextBox passtxt, Button btn, ToolStripMenuItem register)
        {
           
            btn.Text = "Register";
            register.Enabled = false;
            RegisterTime = true;
        }
        public void Register(TextBox usertxt, TextBox passtxt, Button btn, ToolStripMenuItem register)
        {
            if (!string.IsNullOrWhiteSpace(usertxt.Text) && !string.IsNullOrWhiteSpace(passtxt.Text))
            {
                for (int i = 0; i < accounts.usernames.Length; i++)
                {
                    if (accounts.usernames[i] == null || usertxt.Text == accounts.usernames[i])
                    {
                        if (usertxt.Text == accounts.usernames[i])
                        {
                            ExsitName = true;
                            MessageBox.Show("Name Already Exist","Error!");
                            Console.WriteLine("Name Already Exist");
                            break;
                        }
                        else
                        {
                            ExsitName = false;
                            accounts.usernames[i] = usertxt.Text;
                            Console.WriteLine("Register Success!");
                            break;
                        }
                        
                    }
                }
                if (!ExsitName)
                {
                    MessageBox.Show("Username : " + usertxt.Text + "\t\t\nPassword : " + passtxt.Text + "\t\t\n\nWas Registered!", "Register");
                    btn.Text = "Login";
                    register.Enabled = true;
                    RegisterTime = false;
                }
                
            }
            else
            {
                Console.WriteLine("Register Error!");
                MessageBox.Show("You need to enter both user and password", "Error");
            }
           

        }
        public void Login(TextBox usertxt, TextBox passtxt)
        {
            for (int i = 0; i < accounts.usernames.Length; i++)
            {
                if (usertxt.Text == accounts.usernames[i])
                {
                    Logon = true;
                    Console.WriteLine("Login Sucess!");
                    MessageBox.Show("Loged In!","Login");
                    Task.Run(() => Application.Run(new UserMenu(accounts.usernames[i])));
                    Application.Exit();
                    break;
                }
                else
                {
                    Logon = false;
                    
                }
            }
            if (!Logon)
            {
                Console.WriteLine("Wrong ID!");
                MessageBox.Show("Wrong","Login");
                
            }
            
        }

    }
}
