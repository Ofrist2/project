﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;


namespace GitProject
{
   
    public partial class Login : Form
    {
        
        private LoginFormDinamic LoginFormDinamic = new LoginFormDinamic();
        public Login()
        {
            InitializeComponent();
            
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Text Fileds Cleared!");
            LoginFormDinamic.New(UserTxt,PassTxt);
            
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Console.WriteLine("Application Exited!");
        }
        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Info Box Showed!");
            LoginFormDinamic.Info();
            
        }
        private void registerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Register Form is Active");
            LoginFormDinamic.RegisterBtn(UserTxt, PassTxt, LoginBtn, registerToolStripMenuItem);
        }
        private void LoginBtn_Click(object sender, EventArgs e)
        {
            if (LoginFormDinamic.RegisterTime)
            {
                Console.WriteLine("Register...");
                LoginFormDinamic.Register(UserTxt,PassTxt,LoginBtn,registerToolStripMenuItem);
                LoginFormDinamic.New(UserTxt, PassTxt);
                
            }
            else if (!LoginFormDinamic.RegisterTime)
            {
                Console.WriteLine("Login...");
                LoginFormDinamic.Login(UserTxt,PassTxt);
            }
        }
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Task.Run(() => Application.Run(new UserMenu("Admin")));
            Application.Exit();
        }
    }
}
