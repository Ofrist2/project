﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowHeight = 15;
            Console.WindowWidth = 60;
            Console.WriteLine("Program Has Loaded");
            Console.WriteLine("Waiting For User To Enter Login Information");
            Task.Run(() => Application.Run(new Login()));
            

            Console.ReadLine();
        }
    }
}
